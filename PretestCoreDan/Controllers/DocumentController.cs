﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using PretestCoreDan.Models;
using PretestCoreDan.Repository;
using System.Data;
using System.Reflection.Metadata;
using System.Web.Http.ModelBinding;

namespace PretestCoreDan.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public DocumentController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("DocumentList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getDocument()
        {
            var result = _authentication.getDocumentList<ModelDocument>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelDocument document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", document.IDCompany, DbType.Int64);
            dp_param.Add("idcategory", document.IDCategory, DbType.Int64);
            dp_param.Add("name", document.Name, DbType.String);
            dp_param.Add("description", document.Description, DbType.String);
            dp_param.Add("flag", document.Flag, DbType.Int64);
            dp_param.Add("createdby", document.CreatedBy, DbType.Int64);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_CreateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = document });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelDocument document, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("idcompany", document.IDCompany, DbType.Int64);
            dp_param.Add("idcategory", document.IDCategory, DbType.Int64);
            dp_param.Add("name", document.Name, DbType.String);
            dp_param.Add("description", document.Description, DbType.String);
            dp_param.Add("flag", document.Flag, DbType.Int64);
            dp_param.Add("createdby", document.CreatedBy, DbType.Int64);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_UpdateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = document });
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_DeleteDocument", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }
    }
}
