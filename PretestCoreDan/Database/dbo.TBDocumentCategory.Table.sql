USE [PretestDan]
GO
/****** Object:  Table [dbo].[TBDocumentCategory]    Script Date: 04/08/2023 00:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBDocumentCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBDocumentCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TBDocumentCategory]  WITH CHECK ADD  CONSTRAINT [FK_TBDocumentCategory_TBDocumentCategory] FOREIGN KEY([ID])
REFERENCES [dbo].[TBDocumentCategory] ([ID])
GO
ALTER TABLE [dbo].[TBDocumentCategory] CHECK CONSTRAINT [FK_TBDocumentCategory_TBDocumentCategory]
GO
