USE [PretestDan]
GO
/****** Object:  Table [dbo].[TBUser]    Script Date: 04/08/2023 00:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NOT NULL,
	[IDPosition] [int] NOT NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TBCompany1] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[TBCompany] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TBCompany1]
GO
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TBPosition] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[TBPosition] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TBPosition]
GO
