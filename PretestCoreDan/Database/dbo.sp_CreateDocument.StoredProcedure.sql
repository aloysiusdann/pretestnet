USE [PretestDan]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocument]    Script Date: 04/08/2023 00:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocument]
	-- Add the parameters for the stored procedure here
	@idcompany int,
	@idcategory int,
	@name varchar (225),
	@description text,
	@flag int,
	@createdBy int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[TBDocument]
            ([UID]
           ,[IDCompany]
           ,[IDCategory]
           ,[Name]
           ,[Description]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
			(NEWID()
			,@idcompany
			,@idcategory
            ,@name
		    ,@description
		    ,@flag
            ,@CreatedBy
            ,GETDATE())

IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END



GO
