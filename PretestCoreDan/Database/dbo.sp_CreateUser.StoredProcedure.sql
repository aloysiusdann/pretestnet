USE [PretestDan]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateUser]    Script Date: 04/08/2023 00:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateUser]
	-- Add the parameters for the stored procedure here
	@idcompany int,
	@idposition int,
	@name varchar(225),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(50),
	@role varchar(50),
	@flag int,
	@createdby int OUTPUT,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
INSERT INTO[PretestDan].[dbo].[TBUser]
           ([UID]
           ,[IDCompany]
           ,[IDPosition]
           ,[Name]
           ,[Address]
           ,[Email]
           ,[Telephone]
           ,[Username]
           ,[Password]
           ,[Role]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
		(NEWID()
		,@idcompany
		,@idposition
		,@name
		,@address
		,@email
		,@telephone
		,@username
		,CONVERT(VARCHAR(32),HashBytes('MD5',@password),2)
		,@role
		,@flag
		,@createdby
		,GETDATE())
         IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END

GO
