USE [PretestDan]
GO
/****** Object:  StoredProcedure [dbo].[sp_LoginUser]    Script Date: 04/08/2023 00:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_LoginUser]
	@email varchar(50),
	@password varchar (50),
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	SELECT 
			[ID],
			[Email],
			[Username],
			[Password],
			[Role],
			[CreatedBy],
			[CreatedAt]
			FROM TBUser
			WHERE
			[Email] = @email and [Password] = CONVERT(VARCHAR(32), HashBytes('MD5',@password),2)

		IF(@@ROWCOUNT > 0)
		BEGIN
			SET @retVal = 200
		END
		ELSE
		BEGIN
			SET @retVal = 500
		END
		END


GO
