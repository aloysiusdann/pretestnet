USE [PretestDan]
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateCompany]    Script Date: 04/08/2023 00:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar (225),
	@address text,
	@email varchar (50),
	@telephone varchar (14),
	@flag int,
	@createdBy int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [dbo].[TBCompany] SET
           [Name] = @name
		  ,[Address] = @address
		  ,[Email] = @email
		  ,[Telephone] = @telephone
		  ,[Flag] = @flag
          ,[CreatedBy] = @createdBy
     WHERE ID = @id

IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END




GO
