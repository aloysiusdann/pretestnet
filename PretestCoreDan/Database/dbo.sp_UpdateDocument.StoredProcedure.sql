USE [PretestDan]
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocument]    Script Date: 04/08/2023 00:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
	@idcategory int,
	@name varchar (225),
	@description text,
	@flag int,
	@createdBy int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [dbo].[TBDocument] SET
		[IDCompany] = @idcompany
		  ,[IDCategory] = @idcategory
		  ,[Name] = @name
		  ,[Description] = @description
		  ,[Flag] = @flag
          ,[CreatedBy] = @createdBy
     WHERE ID = @id

IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END




GO
