﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreDan.Models
{
    public class ModelDocumentCategory
    {
        [Required]
        public string? Name { get; set; }

        public int? CreatedBy { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
